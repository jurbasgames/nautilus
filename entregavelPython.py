#Questao 1
def progressao(n1,nx,razao):
    res = []
    if razao % 2 == 0:
        #PA
        nAtual = n1
        while nAtual <= nx:
            res.append(nAtual)
            nAtual = nAtual + razao
        return res
    else:
        #PG
        nAtual = n1
        while nAtual <= nx:
            res.append(nAtual)
            nAtual = nAtual*razao
        return res
#Questao 2
def binario(inteiro):
    res = 0
    nb = bin(inteiro)
    for i in nb[2:]:
        res = res + int(i)
    return res

#Questao 3
def bruteforce(quantia=200):
    h=0
    res=[]
    casos = 0
    while h <= quantia/200:
        g=0
        while g <= quantia/100:
            f=0
            while f <= quantia/50:
                e=0
                while e <= quantia/20:
                    d=0
                    while d <= quantia/10:
                        c=0
                        while c <= quantia/5:
                            b=0
                            while b <= quantia/2:
                                a=0
                                while a <= quantia:
                                    total = a + 2*b + 5*c + 10*d + 20*e + 50*f + 100*g + 200*h
                                    if total == quantia:
                                        casos +=1
                                    a+=1
                                b+=1
                            c+=1
                        d+=1
                    e+=1
                f+=1
            g+=1
        h+=1
    return casos 

#Alternativa

def combinacao(quantia):
    moedas = [1, 2, 5, 10, 20, 50, 100, 200]
    lista = [1] + [0] * quantia
    for n in moedas:
        for i in range(n, quantia+1):
            lista[i] += lista[i-n]
    return lista[quantia]

#Questao 4

def dig(tamanho):
    res=0
    i=0
    while i < tamanho:
        res+= i**i
        i+=1
    res=str(res)
    return res[-10::1]
